package com.example.interfaces.impl;

import com.example.interfaces.StudentService;

public class StudentServiceImpl implements StudentService {
    private String collegeName;
    private int rollNo;

    public StudentServiceImpl(String collegeName) {
        this.collegeName = collegeName;
    }

    @Override
    public String getCollegeName() {
        return this.collegeName;
    }

    public void setRollNo(int rollNo) {
        this.rollNo = rollNo;
    }
}
