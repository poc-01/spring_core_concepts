package com.example.start;

import com.example.services.StudentClient;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext(
                "beans.xml");

        StudentClient studentClient = (StudentClient) context.getBean("studentClient");
        String nam = studentClient.getCollegeName();
        System.out.println(nam);
    }
}
