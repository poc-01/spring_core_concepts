package com.example.services;

import com.example.interfaces.StudentService;

public class StudentClient {
    private StudentService studentService;
    public StudentClient(StudentService studentService) {
        this.studentService = studentService;
    }

    public String getCollegeName(){
        return this.studentService.getCollegeName();
    }
}
